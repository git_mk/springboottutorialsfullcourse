package at.itkolleg.fullstack.springi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class SpringiApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context= SpringApplication.run(SpringiApplication.class, args);
        Alien a=context.getBean(Alien.class);
        a.show();
    }

}
